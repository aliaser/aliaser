/*
 * Copyright 2017 Andrew Hayzen <ahayzen@gmail.com>
 *
 * This file is part of aliaser.
 *
 * aliaser is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; version 3.
 *
 * aliaser is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#[macro_use] extern crate aliaserlib;

#[cfg(test)]
#[macro_use] extern crate lazy_static;  // actually only used in tests

mod instance;
mod environment;
mod printer;

use std::collections::BTreeMap;
use std::env;
use std::io;
use std::path::PathBuf;
use std::sync::{Arc, mpsc, Mutex};
use std::{thread, time};

use aliaserlib::error::LibError;

use instance::Instance;
use printer::{Printer, PrinterStdio};

// Starts a logging thread and returns a Sender
// which stops the thread when a packet is sent
fn start_logging_recv_thread<T: Printer>(printer: Arc<Mutex<T>>) -> mpsc::Sender<()> where T: 'static, T: std::marker::Send {
    let (tx, rx) = mpsc::channel();

    thread::spawn(move || {
        loop {
            if let Some((level, data)) = aliaserlib::logging::try_recv() {
                let level_text = match level {
                    aliaserlib::logging::LogLevel::Error => "E",
                    aliaserlib::logging::LogLevel::Warn => "W",
                    aliaserlib::logging::LogLevel::Info => "I",
                    aliaserlib::logging::LogLevel::Debug => "D",
                    aliaserlib::logging::LogLevel::Trace => "T",
                };

                {
                    let printer = printer.lock().unwrap();
                    (*printer).stderr(format!("{}: {}\n", level_text, data));
                }
            } else if rx.try_recv().is_ok() {
                break;
            } else {
                thread::sleep(time::Duration::from_millis(10));
            }
        }
    });

    tx
}

fn main_helper<T: Printer>(printer: Arc<Mutex<T>>, directory: Result<PathBuf, io::Error>, environment: BTreeMap<String, String>, arguments: Vec<String>) -> Result<i32, LibError> {
    // Try to load the current directory
    if let Ok(directory) = directory {
        // Try to create an instance from the directory and arguments and execute it's command
        Instance::new(printer, directory, environment, &arguments[1..])?.auto()
    } else {
        Err(lib_error!("Could not determine current directory, either it does not exist or there are insufficient permissions to access it"))
    }
}

fn main() {
    let printer = Arc::new(Mutex::new(PrinterStdio::new()));

    // Start a thread to perform any logging
    let thread_tx = start_logging_recv_thread(Arc::clone(&printer));

    // Run main_helper with the current environment and exit with the given code
    let code = match main_helper(Arc::clone(&printer), env::current_dir(), env::vars().collect(), env::args().collect()) {
        Ok(code) => code,
        Err(err) => {
            error!("{}", err);
            -1
        }
    };

    thread_tx.send(()).unwrap();  // tell logging thread to stop

    // Wait until thread closes
    while thread_tx.send(()).is_ok() {
        thread::sleep(time::Duration::from_millis(10));
    }

    std::process::exit(code);  // exit with given code
}

#[cfg(test)]
pub mod locks {
    use std::sync::Mutex;

    lazy_static! {
        pub static ref TEST_PRINTER_MUTEX: Mutex<()> = Mutex::new(());
    }
}

#[cfg(test)]
pub mod tests {
    use super::*;

    use printer::PrinterMock;
    use locks::TEST_PRINTER_MUTEX;

    // Helper for reading all the remaining packets in aliaserlib::logging
    fn flush_logging_packets() {
        while aliaserlib::logging::try_recv().is_some() {

        }
    }

    #[test]
    fn test_main_helper() {
        let _guard = TEST_PRINTER_MUTEX.lock().unwrap();

        // Try to run with no arguments (just the program), should get help text
        let printer = Arc::new(Mutex::new(PrinterMock::new()));
        let out = main_helper(Arc::clone(&printer), env::current_dir(), env::vars().collect(), vec!("program".to_owned()));

        // Check that there was not an error
        assert!(out.is_ok());

        // Check that the printer had something in the stdout
        {
            let printer = printer.lock().unwrap();
            assert_eq!(false, (*printer).read_stdout().is_empty());
        }
    }

    #[test]
    fn test_main_helper_invalid_command() {
        let _guard = TEST_PRINTER_MUTEX.lock().unwrap();

        // Try to run a command and workflow that do not exist
        let printer = Arc::new(Mutex::new(PrinterMock::new()));
        let out = main_helper(Arc::clone(&printer), env::current_dir(), env::vars().collect(), vec!("program".to_owned(), "workflow".to_owned(), "command".to_owned()));

        // Check that there was an error
        assert!(out.is_err());

        // Check that the printer had nothing in the stdout
        {
            let printer = printer.lock().unwrap();
            assert_eq!(true, (*printer).read_stdout().is_empty());
        }
    }

    #[test]
    fn test_main_helper_invalid_directory() {
        let _guard = TEST_PRINTER_MUTEX.lock().unwrap();

        // Try to run in a directory that does not exist
        let printer = Arc::new(Mutex::new(PrinterMock::new()));
        let out = main_helper(Arc::clone(&printer), Err(io::Error::new(io::ErrorKind::NotFound, "Invalid directory!")), env::vars().collect(), vec!("program".to_owned()));

        // Check that there was an error
        assert!(out.is_err());

        // Check that the printer had nothing in the stdout
        {
            let printer = printer.lock().unwrap();
            assert_eq!(true, (*printer).read_stdout().is_empty());
        }
    }

    #[test]
    fn test_main_helper_invalid_option() {
        let _guard = TEST_PRINTER_MUTEX.lock().unwrap();

        // Try to run $ aliaser --invalid it should error
        let printer = Arc::new(Mutex::new(PrinterMock::new()));
        let out = main_helper(Arc::clone(&printer), env::current_dir(), env::vars().collect(), vec!("program".to_owned(), "--invalid".to_owned()));

        // Check that there was an error
        assert!(out.is_err());

        // Check that the printer had nothing in the stdout
        {
            let printer = printer.lock().unwrap();
            assert_eq!(true, (*printer).read_stdout().is_empty());
        }
    }

    #[test]
    fn test_start_logging_recv_thread() {
        let _guard = TEST_PRINTER_MUTEX.lock().unwrap();

        // other tests may have caused packets to be sent to aliaserlib::logging
        // so we read all pending packets here
        flush_logging_packets();

        let printer = Arc::new(Mutex::new(PrinterMock::new()));

        // Start spinning thread
        let tx = start_logging_recv_thread(Arc::clone(&printer));

        // Try each printing type
        aliaserlib::logging::enable_logging(aliaserlib::logging::LogLevel::Trace);

        // Trace
        {
            trace!("trace");

            // Ensure that the packet has had time to be processed
            thread::sleep(time::Duration::from_millis(25));

            let printer = printer.lock().unwrap();
            assert_eq!("T: trace\n".to_owned(), (*printer).read_stderr());
            assert!((*printer).read_stdout().is_empty());
        }

        // Debug
        {
            debug!("debug");

            // Ensure that the packet has had time to be processed
            thread::sleep(time::Duration::from_millis(25));

            let printer = printer.lock().unwrap();
            assert_eq!("D: debug\n".to_owned(), (*printer).read_stderr());
            assert!((*printer).read_stdout().is_empty());
        }

        // Info
        {
            info!("info");

            // Ensure that the packet has had time to be processed
            thread::sleep(time::Duration::from_millis(25));

            let printer = printer.lock().unwrap();
            assert_eq!("I: info\n".to_owned(), (*printer).read_stderr());
            assert!((*printer).read_stdout().is_empty());
        }

        // Warn
        {
            warn!("warn");

            // Ensure that the packet has had time to be processed
            thread::sleep(time::Duration::from_millis(25));

            let printer = printer.lock().unwrap();
            assert_eq!("W: warn\n".to_owned(), (*printer).read_stderr());
            assert!((*printer).read_stdout().is_empty());
        }

        // Error
        {
            error!("error");

            // Ensure that the packet has had time to be processed
            thread::sleep(time::Duration::from_millis(25));

            let printer = printer.lock().unwrap();
            assert_eq!("E: error\n".to_owned(), (*printer).read_stderr());
            assert!((*printer).read_stdout().is_empty());
        }

        tx.send(()).unwrap();  // kill the thread

        // Wait for thread to end
        while tx.send(()).is_ok() {
            thread::sleep(time::Duration::from_millis(10));
        }
    }
}
