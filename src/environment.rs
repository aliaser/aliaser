/*
 * Copyright 2017 Andrew Hayzen <ahayzen@gmail.com>
 *
 * This file is part of aliaser.
 *
 * aliaser is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; version 3.
 *
 * aliaser is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
use std::env;
use std::path::PathBuf;

use aliaserlib::logging::LogLevel;

// Environment vars
static ENV_CONFIG: &'static str = "ALIASER_CONFIG_PATH";
static ENV_EXECUTER: &'static str = "ALIASER_EXECUTER";
static ENV_LOGGER: &'static str = "ALIASER_LOG";

/// Provides methods which changes environment variables into relevant data types
pub struct Environment {

}

impl Environment {
    /// Find if any config paths have been set
    ///
    /// Note paths are loaded left to right
    ///
    /// The variable could be like the following
    /// `/sys/path/*.yaml:/usr/path:./.aliaser.yaml:./aliaser`
    pub fn config_paths() -> Vec<PathBuf> {
        match env::var(ENV_CONFIG).as_ref() {
            Ok(paths) if !paths.is_empty() => paths.split(":").map(|path| PathBuf::from(path)).collect(),
            _ => {
                // TODO: hard code some paths
                vec!()
            }
        }
    }

    /// Find if an executer has been chosen to use
    pub fn executer() -> Option<String> {
        match env::var(ENV_EXECUTER).as_ref() {
            Ok(executer) if !executer.is_empty() => Some(executer.to_owned()),
            _ => None
        }
    }

    /// Find if a level of logging has been chosen
    pub fn logging() -> Option<LogLevel> {
        match env::var(ENV_LOGGER).as_ref().map(String::as_ref) {
            Ok("trace") => Some(LogLevel::Trace),
            Ok("debug") => Some(LogLevel::Debug),
            Ok("warn") => Some(LogLevel::Warn),
            Ok("error") => Some(LogLevel::Error),
            Ok("") => None,
            Ok(unknown) => {
                warn!("Unknown option for env var {}: {:?}", ENV_LOGGER, unknown);

                None
            },
            Err(_) => None,
        }
    }
}

#[cfg(test)]
pub mod locks {
   use std::sync::Mutex;

    lazy_static! {
        pub static ref TEST_CONFIG_MUTEX: Mutex<()> = Mutex::new(());
        pub static ref TEST_EXECUTER_MUTEX: Mutex<()> = Mutex::new(());
        pub static ref TEST_LOGGER_MUTEX: Mutex<()> = Mutex::new(());
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    use environment::locks::{TEST_CONFIG_MUTEX, TEST_EXECUTER_MUTEX, TEST_LOGGER_MUTEX};
    use locks::TEST_PRINTER_MUTEX;

    #[test]
    fn test_config_path() {
        let _guard = TEST_CONFIG_MUTEX.lock().unwrap();

        env::set_var(ENV_CONFIG, "/sys/path:/usr/path:.local/path");
        let paths = Environment::config_paths();
        assert_eq!(3, paths.len());
        assert_eq!(PathBuf::from("/sys/path"), paths[0]);
        assert_eq!(PathBuf::from("/usr/path"), paths[1]);
        assert_eq!(PathBuf::from(".local/path"), paths[2]);
    }

    #[test]
    fn test_config_path_empty() {
        let _guard = TEST_CONFIG_MUTEX.lock().unwrap();

        env::set_var(ENV_CONFIG, "");
        assert_eq!(0, Environment::config_paths().len());
    }

    #[test]
    fn test_config_path_none() {
        let _guard = TEST_CONFIG_MUTEX.lock().unwrap();

        env::remove_var(ENV_CONFIG);
        assert_eq!(0, Environment::config_paths().len());
    }

    #[test]
    fn test_executer() {
        let _guard = TEST_EXECUTER_MUTEX.lock().unwrap();

        env::set_var(ENV_EXECUTER, "test");
        assert!(Environment::executer().is_some());
        assert_eq!("test".to_owned(), Environment::executer().unwrap());

        env::remove_var(ENV_EXECUTER);
    }

    #[test]
    fn test_executer_empty() {
        let _guard = TEST_EXECUTER_MUTEX.lock().unwrap();

        env::set_var(ENV_EXECUTER, "");
        assert!(Environment::executer().is_none());

        env::remove_var(ENV_EXECUTER);
    }

    #[test]
    fn test_executer_none() {
        let _guard = TEST_EXECUTER_MUTEX.lock().unwrap();

        env::remove_var(ENV_EXECUTER);
        assert!(Environment::executer().is_none());
    }

    // Logging tests lock printer, as printing tests may require certain level

    #[test]
    fn test_logging_debug() {
        let _guard = TEST_LOGGER_MUTEX.lock().unwrap();
        let _guard_printer = TEST_PRINTER_MUTEX.lock().unwrap();

        env::set_var(ENV_LOGGER, "debug");
        assert!(Environment::logging().is_some());
        assert_eq!(LogLevel::Debug, Environment::logging().unwrap());

        env::remove_var(ENV_LOGGER);
    }

    #[test]
    fn test_logging_empty() {
        let _guard = TEST_LOGGER_MUTEX.lock().unwrap();
        let _guard_printer = TEST_PRINTER_MUTEX.lock().unwrap();

        env::set_var(ENV_LOGGER, "");
        assert!(Environment::logging().is_none());

        env::remove_var(ENV_LOGGER);
    }

    #[test]
    fn test_logging_error() {
        let _guard = TEST_LOGGER_MUTEX.lock().unwrap();
        let _guard_printer = TEST_PRINTER_MUTEX.lock().unwrap();

        env::set_var(ENV_LOGGER, "error");
        assert!(Environment::logging().is_some());
        assert_eq!(LogLevel::Error, Environment::logging().unwrap());

        env::remove_var(ENV_LOGGER);
    }

    #[test]
    fn test_logging_none() {
        let _guard = TEST_LOGGER_MUTEX.lock().unwrap();
        let _guard_printer = TEST_PRINTER_MUTEX.lock().unwrap();

        env::remove_var(ENV_LOGGER);
        assert!(Environment::logging().is_none());
    }

    #[test]
    fn test_logging_trace() {
        let _guard = TEST_LOGGER_MUTEX.lock().unwrap();
        let _guard_printer = TEST_PRINTER_MUTEX.lock().unwrap();

        env::set_var(ENV_LOGGER, "trace");
        assert!(Environment::logging().is_some());
        assert_eq!(LogLevel::Trace, Environment::logging().unwrap());

        env::remove_var(ENV_LOGGER);
    }

    #[test]
    fn test_logging_unknown() {
        let _guard = TEST_LOGGER_MUTEX.lock().unwrap();
        let _guard_printer = TEST_PRINTER_MUTEX.lock().unwrap();

        env::set_var(ENV_LOGGER, "invalid");
        assert!(Environment::logging().is_none());

        env::remove_var(ENV_LOGGER);
    }


    #[test]
    fn test_logging_warn() {
        let _guard = TEST_LOGGER_MUTEX.lock().unwrap();
        let _guard_printer = TEST_PRINTER_MUTEX.lock().unwrap();

        env::set_var(ENV_LOGGER, "warn");
        assert!(Environment::logging().is_some());
        assert_eq!(LogLevel::Warn, Environment::logging().unwrap());

        env::remove_var(ENV_LOGGER);
    }
}
