/*
 * Copyright 2017 Andrew Hayzen <ahayzen@gmail.com>
 *
 * This file is part of aliaser.
 *
 * aliaser is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; version 3.
 *
 * aliaser is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
use std::collections::BTreeMap;
use std::path::PathBuf;
use std::sync::{Arc, Mutex};
use std::vec::Vec;

use aliaserlib::{Config, ConfigBuilder, LibError, Pipeline, Request, Runner, OutputBlocking};
use aliaserlib::logging::{enable_logging, LogLevel};

// FIXME: do we need special version stuff? or just aliaserlib.MAJOR .MINOR ?
use aliaserlib::info;
use aliaserlib::runner::executer::ExecuterType;

use environment::Environment;
use printer::Printer;


/// Different listing output modes for this app
#[derive(Debug, PartialEq)]
enum InstanceModeList {
    Backends,
    Defaults,
    Workflows,
}

/// Different output modes for this app
#[derive(Debug, PartialEq)]
enum InstanceMode {
    /// Try to execute the command in the arguments
    Command,
    /// Listing of information from the configuration
    List(InstanceModeList),
    /// Show the help message
    Help,
    /// Show the version
    Version,
}

pub struct Instance<T> {
    /// Information from the environment

    /// List of arguments given at the command line
    arguments: Vec<String>,

    /// The directory which to run this command
    directory: PathBuf,

    /// Options that the user can override config

    /// The base environment in which commands will be executed with
    environment: BTreeMap<String, String>,

    /// Which executer to use, eg "cmd", "mock" or None to read from config
    // TODO: maybe executer becomes enum ?
    // then we remove Runner::default_executer_for_key() and just use enums here?
    executer: Option<String>,

    /// Options that the user can choose

    /// The logging level, error, warn, info, debug, trace or None (warn)
    logging: Option<LogLevel>,

    /// The mode of this instance
    mode: InstanceMode,

    /// The config paths to load
    paths: Vec<PathBuf>,

    /// The printer to use for this instance
    print: Arc<Mutex<T>>,  // TODO: rename to printer ?
}

impl<T: Printer> Instance<T> {
    /// From the loaded instance decide what to do
    pub fn auto(&self) -> Result<i32, LibError> {
        // From mode decide what to do
        match self.mode {
            InstanceMode::Command => self.run_command(),
            InstanceMode::List(ref mode) => self.print_list(mode),
            InstanceMode::Help => self.print_help(),
            InstanceMode::Version => self.print_version(),
        }
    }

    pub fn new(printer: Arc<Mutex<T>>, directory: PathBuf, environment: BTreeMap<String, String>, args: &[String]) -> Result<Self, LibError> {
        let mut instance = Self {
            arguments: vec!(),
            directory: directory,
            executer: None,
            environment: environment,
            logging: Environment::logging(),
            mode: InstanceMode::Help,
            paths: Environment::config_paths(),
            print: printer,
        };

        // Check if an executer has been set
        instance.executer = Environment::executer();

        // Set logging level if one has been set
        // Either from environment variable or --verbose (verbose overrides)
        if let Some(level) = instance.logging.clone() {
            enable_logging(level);
        } else {
            // Fallback to log level of warn
            enable_logging(LogLevel::Warn);
        }

        // Read arguments from command line (can these override environment)
        // Note first is the program, eg aliaser, so we skip it
        instance.read_arguments(args)?;

        // Set logging level if one has been set
        // Either from environment variable or --verbose (verbose overrides)
        if let Some(level) = instance.logging.clone() {
            enable_logging(level);
        } else {
            // Fallback to log level of warn
            enable_logging(LogLevel::Warn);
        }

        Ok(instance)
    }
}

// TODO: could parts that interface with aliaserlib command be split out ?
// load_config
// run_command
// ?
// does that help tests?

impl<T: Printer> Instance<T> {
    /// Find an executer from either (in this order)
    ///   * The instance executer
    ///   * The configuration default executer
    ///   * The library default executer
    fn find_executer(&self, config: &Config) -> Result<Box<ExecuterType>, LibError> {
        match self.executer {
            Some(ref key) => {
                match Runner::default_executer_for_key(key) {
                    Some(exec) => Ok(exec),
                    None => Err(lib_error!(format!("Could not find executer: {:?}", key)))
                }
            },
            None => {
                match config.defaults().executer() {
                    Some(key) if !key.is_empty() => {
                        match Runner::default_executer_for_key(key) {
                            Some(exec) => Ok(exec),
                            None => Err(lib_error!(format!("Could not find executer: {:?}", key)))
                        }
                    },
                    _ => Ok(Runner::default_executer())
                }
            }
        }
    }

    /// Uses paths to build a Config
    fn load_config(&self) -> Result<Config, LibError> {
        // Use ConfigBuilder to build a Config
        let mut builder = ConfigBuilder::new();

        for path in &self.paths {
            // TODO: needs to resolve any relative paths
            // relative paths could be like ./.aliaser.yaml or ./aliaser
            if path.is_relative() {

            } else {
                builder.add_path(&path)?;
            }
        }

        Ok(builder.to_config()?)
    }

    /// Print the help message
    fn print_help(&self) -> Result<i32, LibError> {
        let printer = self.print.lock().unwrap();
        printer.stdout(format!(
"Usage:
  aliaser [OPTIONS] <COMMANDS> [CMD_OPTIONS]

Aliaser - version: {}
  - a tool for aliasing commands and workflows

Applications options:
--clean-env         Only exposes PATH to the sub commands
--dry-run           Set ALIASER_EXECUTER=mock
--help              Show this message and exit
--list-backends     List the loaded backends
--list-defaults     List the loaded default settings
--list-workflows    List the loaded workflows
--verbose           Set ALIASER_LOG=debug
--version           Print the version and exit

Available commands:
  <WORKFLOW_CMD> [WORKFLOW_OPTIONS]
  workflow <WORKFLOW_CMD> [WORKFLOW_OPTIONS]
  workflow:implementation <WORKFLOW_CMD> [WORKFLOW_OPTIONS]
",
        info::version()));

        Ok(0)
    }

    /// Print a list of information defined by the given mode
    fn print_list(&self, mode: &InstanceModeList) -> Result<i32, LibError> {
        let config = self.load_config()?;

        match mode {
            &InstanceModeList::Backends => {
                for backend in config.backends() {
                    let printer = self.print.lock().unwrap();
                    (*printer).stdout(format!("{}\n", backend));
                }
            }
            &InstanceModeList::Defaults => {
                let printer = self.print.lock().unwrap();
                if let Some(exec) = config.defaults().executer() {
                    (*printer).stdout(format!("Executer: {}\n", exec));
                } else {
                    (*printer).stdout("Executer: auto\n".to_owned())
                }
                (*printer).stdout(format!("Env: {:?}\n", config.defaults().env()));
                (*printer).stdout(format!("Workflow: {:?}\n", config.defaults().workflows()));  // TODO: make string using workflow(key)
            },
            &InstanceModeList::Workflows => {
                for workflow in config.workflows() {
                    let printer = self.print.lock().unwrap();
                    (*printer).stdout(format!("{}\n", workflow));
                }
            }
        }

        Ok(0)
    }

    /// Print the version of the library
    fn print_version(&self) -> Result<i32, LibError> {
        let printer = self.print.lock().unwrap();
        (*printer).stdout(format!("{}\n", info::version()));

        Ok(0)
    }

    /// Read the given arguments and determine Instance values
    fn read_arguments(&mut self, args: &[String]) -> Result<&mut Self, LibError> {
        match args.first().map(String::as_ref) {
            Some("--clean-env") => {
                // Copy env so we can re-set things after the clear
                let environment = self.environment.clone();

                self.environment.clear();

                // If there is a PATH then re-set it
                if let Some(path) = environment.get("PATH") {
                    self.environment.insert("PATH".to_owned(), path.to_owned());
                }

                self.read_arguments(&args[1..])
            },
            Some("--dry-run") => {
                self.executer = Some("mock".to_owned());

                self.read_arguments(&args[1..])
            },
            Some("--help") => {
                self.mode = InstanceMode::Help;

                Ok(self)
            },
            Some("--list-backends") => {
                self.mode = InstanceMode::List(InstanceModeList::Backends);

                Ok(self)
            },
            Some("--list-defaults") => {
                self.mode = InstanceMode::List(InstanceModeList::Defaults);

                Ok(self)
            },
            Some("--list-workflows") => {
                self.mode = InstanceMode::List(InstanceModeList::Workflows);

                Ok(self)
            },
            Some("--verbose") => {
                // Set level to debug if no level is set or is less than Debug
                self.logging = match self.logging.take() {
                    Some(level) => {
                        if level < LogLevel::Debug {
                            Some(LogLevel::Debug)
                        } else {
                            Some(level)
                        }
                    },
                    None => Some(LogLevel::Debug),
                };

                self.read_arguments(&args[1..])
            },
            Some("--version") => {
                self.mode = InstanceMode::Version;

                Ok(self)
            },
            // User tried to use an internal option but it wasn't recognised
            Some(arg) if arg.starts_with("--") => {
                Err(lib_error!(format!("Unknown internal optional argument: {:?}", arg)))
            },
            // This should be the workflow to use for the command, so save the
            // remaining arguments and set to command mode.
            Some(_) => {
                self.arguments = args.to_vec();
                self.mode = InstanceMode::Command;

                Ok(self)
            },
            // Mode defaults to Help so we can stop as it has already been set
            None => Ok(self)
        }
    }

    /// Try to run the command in the instance
    fn run_command(&self) -> Result<i32, LibError> {
        // Build the Config
        let config = self.load_config()?;

        // Build the request from command line arguments and current working directory
        let request = Request::from(&config, &self.directory, &self.environment, &self.arguments)?;

        // Build the pipeline
        let mut pipeline = Pipeline::from(&config, &request);
        pipeline.build(&Pipeline::default_builders())?;

        // Pick executer either from instance or then config
        let executer = self.find_executer(&config)?;

        // Build the runner and output
        let mut runner = Runner::new(&pipeline, executer);

        // Read the output
        // in it's own scope so we can use runner later
        {
            let output = OutputBlocking::new(&mut runner);

            for packet in output {
                if let Some(data) = packet.stderr {
                    let printer = self.print.lock().unwrap();
                    (*printer).stderr(format!("{}", data));
                }

                if let Some(data) = packet.stdout {
                    let printer = self.print.lock().unwrap();
                    (*printer).stdout(format!("{}", data));
                }
            }
        }

        // If there is an error then show it
        if let Some(error) = runner.take_error() {
            return Err(error);
        }

        // If there is a status code then use it, otherwise exit -1;
        match runner.status() {
            Some(code) => Ok(code),
            None => Ok(-1),
        }
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    use environment::locks::{TEST_EXECUTER_MUTEX, TEST_LOGGER_MUTEX};
    use locks::TEST_PRINTER_MUTEX;
    use printer::PrinterMock;

    // TODO: test_auto
    // TODO: test_find_executer
    // TODO: test_load_config
    // TODO: test_run_command

    #[test]
    fn test_new() {
        let _guard_exec = TEST_EXECUTER_MUTEX.lock().unwrap();
        let _guard_logger = TEST_LOGGER_MUTEX.lock().unwrap();

        // Setup a blank instance and check the state of things

        let printer = Arc::new(Mutex::new(PrinterMock::new()));
        let mut environment = BTreeMap::new();
        environment.insert("a".to_owned(), "b".to_owned());
        let result = Instance::new(printer, PathBuf::from("/tmp"), environment, &[]);

        // Should have created OK
        assert!(result.is_ok());
        let instance = result.unwrap();

        // Check options
        assert!(instance.arguments.is_empty());
        assert_eq!(PathBuf::from("/tmp"), instance.directory);
        assert!(instance.environment.contains_key("a"));
        assert_eq!("b", instance.environment.get("a").unwrap());
        assert!(instance.executer.is_none());
        assert!(instance.logging.is_none());
        assert_eq!(InstanceMode::Help, instance.mode);
    }

    #[test]
    fn test_read_arguments_clean_env() {
        // Check that "--clean-env" is interpreted correctly
        let printer = Arc::new(Mutex::new(PrinterMock::new()));
        let mut environment = BTreeMap::new();
        environment.insert("a".to_owned(), "b".to_owned());
        environment.insert("PATH".to_owned(), "/tmp/path".to_owned());
        let mut instance = Instance::new(printer, PathBuf::from("/tmp"), environment, &[]).unwrap();

        assert!(instance.read_arguments(&["--clean-env".to_owned()]).is_ok());

        // 'a' should be removed
        assert!(!instance.environment.contains_key("a"));

        // PATH should still exist
        assert!(instance.environment.contains_key("PATH"));
        assert_eq!("/tmp/path", instance.environment.get("PATH").unwrap());
    }

    #[test]
    fn test_read_arguments_command() {
        // Check that "workflow command" is interpreted correctly
        let printer = Arc::new(Mutex::new(PrinterMock::new()));
        let mut instance = Instance::new(printer, PathBuf::from("/tmp"), BTreeMap::new(), &[]).unwrap();

        assert!(instance.read_arguments(&["wfl".to_owned(), "cmd".to_owned()]).is_ok());

        assert_eq!(InstanceMode::Command, instance.mode);
        assert_eq!(vec!("wfl".to_owned(), "cmd".to_owned()), instance.arguments);
    }

    #[test]
    fn test_read_arguments_continue() {
        // Check options that should keep reading

        let printer = Arc::new(Mutex::new(PrinterMock::new()));
        let mut instance = Instance::new(printer, PathBuf::from("/tmp"), BTreeMap::new(), &[]).unwrap();
        assert_eq!(InstanceMode::Help, instance.mode);

        // --dry-run should allow --version to change mode
        assert!(instance.read_arguments(&["--dry-run".to_owned(), "--version".to_owned()]).is_ok());
        assert_eq!(InstanceMode::Version, instance.mode);

        instance.mode = InstanceMode::Help;
        assert_eq!(InstanceMode::Help, instance.mode);

        // --verbose should allow --version to change mode
        assert!(instance.read_arguments(&["--verbose".to_owned(), "--version".to_owned()]).is_ok());
        assert_eq!(InstanceMode::Version, instance.mode);

        instance.mode = InstanceMode::Help;
        assert_eq!(InstanceMode::Help, instance.mode);

    }

    #[test]
    fn test_read_arguments_dry_run() {
        // Check that --dry-run is interpreted correctly
        let printer = Arc::new(Mutex::new(PrinterMock::new()));
        let mut instance = Instance::new(printer, PathBuf::from("/tmp"), BTreeMap::new(), &[]).unwrap();

        assert!(instance.read_arguments(&["--dry-run".to_owned()]).is_ok());

        assert!(instance.executer.is_some());
        assert_eq!("mock", instance.executer.unwrap());
    }

    #[test]
    fn test_read_arguments_empty() {
        // Check that no arguments is valid
        let printer = Arc::new(Mutex::new(PrinterMock::new()));
        let mut instance = Instance::new(printer, PathBuf::from("/tmp"), BTreeMap::new(), &[]).unwrap();

        assert!(instance.read_arguments(&[]).is_ok());
    }

    #[test]
    fn test_read_arguments_halt_loop() {
        // Check options that should stop reading further arguments

        let printer = Arc::new(Mutex::new(PrinterMock::new()));
        let mut instance = Instance::new(printer, PathBuf::from("/tmp"), BTreeMap::new(), &[]).unwrap();
        assert!(instance.executer.is_none());

        // Command should not leak to --dry-run
        assert!(instance.read_arguments(&["command".to_owned(), "--dry-run".to_owned()]).is_ok());
        assert!(instance.executer.is_none());

        // Help should not leak to --dry-run
        assert!(instance.read_arguments(&["--help".to_owned(), "--dry-run".to_owned()]).is_ok());
        assert!(instance.executer.is_none());

        // List backends should not leak to --dry-run
        assert!(instance.read_arguments(&["--list-backends".to_owned(), "--dry-run".to_owned()]).is_ok());
        assert!(instance.executer.is_none());

        // List defaults should not leak to --dry-run
        assert!(instance.read_arguments(&["--list-defaults".to_owned(), "--dry-run".to_owned()]).is_ok());
        assert!(instance.executer.is_none());

        // List workflows should not leak to --dry-run
        assert!(instance.read_arguments(&["--list-workflows".to_owned(), "--dry-run".to_owned()]).is_ok());
        assert!(instance.executer.is_none());

        // Version should not leak to --dry-run
        assert!(instance.read_arguments(&["--version".to_owned(), "--dry-run".to_owned()]).is_ok());
        assert!(instance.executer.is_none());
    }

    #[test]
    fn test_read_arguments_help() {
        // Check that --help is interpreted correctly
        let printer = Arc::new(Mutex::new(PrinterMock::new()));
        let mut instance = Instance::new(printer, PathBuf::from("/tmp"), BTreeMap::new(), &[]).unwrap();

        // Set mode to something else so we see a change
        instance.mode = InstanceMode::Version;

        assert!(instance.read_arguments(&["--help".to_owned()]).is_ok());

        assert_eq!(InstanceMode::Help, instance.mode);
    }

    #[test]
    fn test_read_arguments_invalid() {
        // Check that --invalid causes an error
        let printer = Arc::new(Mutex::new(PrinterMock::new()));
        let mut instance = Instance::new(printer, PathBuf::from("/tmp"), BTreeMap::new(), &[]).unwrap();

        assert!(instance.read_arguments(&["--invalid".to_owned()]).is_err());
    }

    #[test]
    fn test_read_arguments_list_backends() {
        // Check that --list-backends is interpreted correctly
        let printer = Arc::new(Mutex::new(PrinterMock::new()));
        let mut instance = Instance::new(printer, PathBuf::from("/tmp"), BTreeMap::new(), &[]).unwrap();

        assert!(instance.read_arguments(&["--list-backends".to_owned()]).is_ok());

        assert_eq!(InstanceMode::List(InstanceModeList::Backends), instance.mode);
    }

    #[test]
    fn test_read_arguments_list_defaults() {
        // Check that --list-defaults is interpreted correctly
        let printer = Arc::new(Mutex::new(PrinterMock::new()));
        let mut instance = Instance::new(printer, PathBuf::from("/tmp"), BTreeMap::new(), &[]).unwrap();

        assert!(instance.read_arguments(&["--list-defaults".to_owned()]).is_ok());

        assert_eq!(InstanceMode::List(InstanceModeList::Defaults), instance.mode);
    }

    #[test]
    fn test_read_arguments_list_workflows() {
        // Check that --list-workflows is interpreted correctly
        let printer = Arc::new(Mutex::new(PrinterMock::new()));
        let mut instance = Instance::new(printer, PathBuf::from("/tmp"), BTreeMap::new(), &[]).unwrap();

        assert!(instance.read_arguments(&["--list-workflows".to_owned()]).is_ok());

        assert_eq!(InstanceMode::List(InstanceModeList::Workflows), instance.mode);
    }

    #[test]
    fn test_read_arguments_verbose() {
        // Check that --verbose is interpreted correctly
        let printer = Arc::new(Mutex::new(PrinterMock::new()));
        let mut instance = Instance::new(printer, PathBuf::from("/tmp"), BTreeMap::new(), &[]).unwrap();

        assert!(instance.read_arguments(&["--verbose".to_owned()]).is_ok());

        assert!(instance.logging.is_some());
        assert_eq!(LogLevel::Debug, instance.logging.unwrap());
    }

    #[test]
    fn test_read_arguments_version() {
        // Check that --version is interpreted correctly
        let printer = Arc::new(Mutex::new(PrinterMock::new()));
        let mut instance = Instance::new(printer, PathBuf::from("/tmp"), BTreeMap::new(), &[]).unwrap();

        assert!(instance.read_arguments(&["--version".to_owned()]).is_ok());

        assert_eq!(InstanceMode::Version, instance.mode);
    }

    #[test]
    fn test_print_help() {
        let _guard = TEST_PRINTER_MUTEX.lock().unwrap();

        let printer = Arc::new(Mutex::new(PrinterMock::new()));
        let instance = Instance::new(Arc::clone(&printer), PathBuf::from("/tmp"), BTreeMap::new(), &[]).unwrap();

        let result = instance.print_help();

        // Check was Ok and status code is zero
        assert!(result.is_ok());
        assert_eq!(0, result.unwrap());

        // Check that something was written to the stdout
        {
            let printer = printer.lock().unwrap();
            assert!(!(*printer).read_stdout().is_empty());
        }
    }

    #[test]
    fn test_print_version() {
        let _guard = TEST_PRINTER_MUTEX.lock().unwrap();

        let printer = Arc::new(Mutex::new(PrinterMock::new()));
        let instance = Instance::new(Arc::clone(&printer), PathBuf::from("/tmp"), BTreeMap::new(), &[]).unwrap();

        let result = instance.print_version();

        // Check was Ok and status code is zero
        assert!(result.is_ok());
        assert_eq!(0, result.unwrap());

        // Check that something was written to the stdout
        {
            let printer = printer.lock().unwrap();
            assert!(!(*printer).read_stdout().is_empty());
        }
    }
}
