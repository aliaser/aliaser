/*
 * Copyright 2017 Andrew Hayzen <ahayzen@gmail.com>
 *
 * This file is part of aliaser.
 *
 * aliaser is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; version 3.
 *
 * aliaser is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
use printer::Printer;
use std::sync::mpsc;

/// [`Printer`] which uses a channel to store the stderr and stdout, so that
/// the data can be retrieved at a later time.
///
/// This is useful for tests as the data is not send to the real stderr
/// or stdout. Instead the test can use the read methods to check the output.
///
/// [`Printer`]: trait.Printer.html
pub struct PrinterMock {
    stderr_rx: mpsc::Receiver<String>,
    stderr_tx: mpsc::Sender<String>,

    stdout_rx: mpsc::Receiver<String>,
    stdout_tx: mpsc::Sender<String>,
}

impl PrinterMock {
    pub fn new() -> Self {
        let (stderr_tx, stderr_rx) = mpsc::channel();
        let (stdout_tx, stdout_rx) = mpsc::channel();

        Self {
            stderr_rx: stderr_rx,
            stderr_tx: stderr_tx,

            stdout_rx: stdout_rx,
            stdout_tx: stdout_tx,
        }
    }

    /// Read all the data waiting in stderr
    pub fn read_stderr(&self) -> String {
        let mut data = "".to_owned();

        loop {
            if let Ok(ref packet) = self.stderr_rx.try_recv() {
                data.push_str(packet);
            } else {
                break;
            }
        }

        data
    }

    /// Read all the data waiting in stdout
    pub fn read_stdout(&self) -> String {
        let mut data = "".to_owned();

        loop {
            if let Ok(ref packet) = self.stdout_rx.try_recv() {
                data.push_str(packet);
            } else {
                break;
            }
        }

        data
    }
}

impl Printer for PrinterMock {
    /// Push data to our stderr channel so tests can read later
    fn stderr(&self, data: String) {
        // FIXME: what should happen if these do fail?
        self.stderr_tx.send(data).unwrap();
    }

    /// Push data to out stdout channel so tests can read later
    fn stdout(&self, data: String) {
        // FIXME: what should happen if these do fail?
        self.stdout_tx.send(data).unwrap();
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_stderr() {
        let printer = PrinterMock::new();

        printer.stderr("a".to_owned());
        printer.stderr("b".to_owned());

        assert_eq!("ab".to_owned(), printer.read_stderr());
        assert_eq!("".to_owned(), printer.read_stdout());
    }

    #[test]
    fn test_stdout() {
        let printer = PrinterMock::new();

        printer.stdout("a".to_owned());
        printer.stdout("b".to_owned());

        assert_eq!("".to_owned(), printer.read_stderr());
        assert_eq!("ab".to_owned(), printer.read_stdout());
    }
}
