/*
 * Copyright 2017 Andrew Hayzen <ahayzen@gmail.com>
 *
 * This file is part of aliaser.
 *
 * aliaser is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; version 3.
 *
 * aliaser is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#[cfg(test)]
mod mock;

mod stdio;

pub trait Printer {
    fn stderr(&self, data: String);
    fn stdout(&self, data: String);
}

#[cfg(test)]
pub use printer::mock::PrinterMock;


pub use printer::stdio::PrinterStdio;
