/*
 * Copyright 2017 Andrew Hayzen <ahayzen@gmail.com>
 *
 * This file is part of aliaser.
 *
 * aliaser is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; version 3.
 *
 * aliaser is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
use std::io;
use std::io::Write;

use printer::Printer;

pub struct PrinterStdio {

}

impl PrinterStdio {
    pub fn new() -> Self {
        Self {

        }
    }
}

impl Printer for PrinterStdio {
    /// Write to stderr
    fn stderr(&self, data: String) {
        let stderr = io::stderr();
        let mut handle = stderr.lock();

        // FIXME: what should happen if these do fail?
        handle.write(data.as_bytes()).unwrap();

        // FIXME: should we flush?
        handle.flush().unwrap();
    }

    /// Write to stdout
    fn stdout(&self, data: String) {
        let stdout = io::stdout();
        let mut handle = stdout.lock();

        // FIXME: what should happen if these do fail?
        handle.write(data.as_bytes()).unwrap();

        // FIXME: should we flush?
        handle.flush().unwrap();
    }
}
