[![build status](https://gitlab.com/aliaser/aliaser/badges/master/build.svg)](https://gitlab.com/aliaser/aliaser/commits/master)
[![coverage report](https://gitlab.com/aliaser/aliaser/badges/master/coverage.svg)](https://gitlab.com/aliaser/aliaser/commits/master)

Aliaser is a console based application using aliaserlib.

Note: aliaser is still under heavy development and is not recommended for use yet!

# Scenario

Ever been confused by differences between programs that do the same job.

Eg to 'uncommit' something in a VCS, to simply undo the 'act' of commiting. In bzr it is `bzr uncommit` however it git it is `git reset --soft HEAD^` and in hg it is `hg rollback` or `hg strip -r -1 -k`.

What if you could simply do `a vcs:git uncommit` or `a vcs:hg uncommit` or as a VCS can be determined by the presence of files `a vcs uncommit` and the command is automatically converted and run for you!

This is a console application which aims to provide this solution, by using simple human readable configuration files.

This console application makes use of the shared library [aliaserlib](http://gitlab.com/aliaser/aliaserlib) which interprets the configurations file.

# Building

```
cargo build
```

# Running

```
cargo run -- --help
```

# Testing

```
./run_tests.sh
```

